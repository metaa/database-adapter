import * as lib from '.'
import { IDatabaseAdapterConstructor } from './interface/IDatabaseAdapterConstructor'
import { IDatabaseAdapterConfig } from './interface/IDatabaseAdapterConfig'


/**
 * The abstract generic class for database adapters.
 * @template T
 */
export abstract class AbstractDatabaseAdapter<T> {
	/**
	 * Registers the database adapter and returns the name (identifier) it chose.
	 * @param {string} name
	 * @param {IDatabaseAdapterConstructor<any>} adapterClass
	 * @returns {string}
	 */
	protected static register(name: string, adapterClass: IDatabaseAdapterConstructor<any>) {
		lib.defineAdapter(name, adapterClass)
		return name
	}

	/**
	 * The configuration for the database adapter.
	 */
	protected _config: IDatabaseAdapterConfig

	/**
	 * The database module's database object that can be used for raw operations.
	 */
	protected _handle: T


	protected constructor(config: IDatabaseAdapterConfig) {
		this._config = config
	}

	/**
	 * Attempts to connect to the database.
	 * This should throw an Error when the connection couldn't be established.
	 */
	public abstract async connect(): Promise<void>

	/**
	 * Creates the database (schema).
	 */
	public abstract async create(): Promise<void>

	/**
	 * Attempts to repair the database.
	 * This could be recreating the schema (calling `create`) / file (in a NoSQL case).
	 */
	public abstract async repair(): Promise<void>

	/**
	 * Should validate thath the database's schema is intact.
	 */
	public abstract async validate(): Promise<void>

	/**
	 * This method defines what to do when connections to the database fail.
	 */
	public abstract async handleUnreachable(): Promise<void>

	/**
	 * Returns a connected state.
	 * The adapter defines, when it's "connected".
	 */
	public abstract get connected(): boolean

	/**
	 * Returns the handle.
	 */
	protected get handle(): T {
		return this._handle
	}

	/**
	 * Sets the handle.
	 * @param {T} handle The handle to set
	 */
	protected set handle(handle: T) {
		this._handle = handle
	}

	/**
	 * Returns the adapter's configuration.
	 * @returns {IDatabaseAdapterConfig}
	 */
	protected get config(): IDatabaseAdapterConfig {
		return this._config
	}
}






