import * as path from 'path'
import { AbstractDatabaseAdapter } from './AbstractDatabaseAdapter'
import { IDatabaseAdapterConfig } from './interface/IDatabaseAdapterConfig'
import { IDatabaseAdapterConstructor } from './interface/IDatabaseAdapterConstructor'
import { IDatabaseAdapterInfo } from './interface/IDatabaseAdapterInfo'


const loadPkg = require('load-pkg')
const resolveDep = require('resolve-dep')

const databaseAdapters: {
	[key: string]: IDatabaseAdapterConstructor<any>
} = {}


/**
 * Registers an adapter to allow using it by passing its `name` to the init1 function later on.
 * @param {string} name The identifier for this adapter
 * @param {IDatabaseAdapterConstructor<T>} adapterClass The class of your adapter
 * @returns {boolean}
 */
export function defineAdapter(name: string, adapterClass: IDatabaseAdapterConstructor<any>): void {
	databaseAdapters[name] = adapterClass
}



function loadPluginsInternal(moduleName: string, dry: boolean = false): string[] {
	const plugins: string[] = resolveDep(`${moduleName}-*`)
		.map((plugin: string) => path.basename(path.dirname(plugin)))

	if(!dry) {
		plugins.forEach(x => require(x))
	}

	return plugins
}

/**
 * Loads all locally available adapter plugins. (async)
 * @param {boolean} dry Whether it should only return the list of plugins instead of automatically loading them.
 * @return {Promise<string[]>} A promise returning a list of plugins that can be loaded.
 */
export async function loadPlugins(dry: boolean = false): Promise<string[]> {
	return new Promise((resolve: (plugins: string[]) => any, reject: (err: any) => any) => {
		loadPkg(__dirname, (err: any, pkg: any) => {
			if(err) return reject(err)

			resolve(loadPluginsInternal(pkg.name, dry))
		})
	})
}

/**
 * Loads all locally available adapter plugins. (sync)
 * @param {boolean} dry Whether it should only return the list of plugins instead of automatically loading them.
 * @return {string[]} A list of plugins that can be loaded.
 */
export function loadPluginsSync(dry: boolean = false): string[] {
	return loadPluginsInternal(loadPkg.sync(__dirname).name, dry)
}


/**
 * Creates a handle for the database adapter and configures it.
 * @param {C} config The configuration that specifies the adapter's identifier and its configuration
 * @returns {Promise<AbstractDatabaseAdapter<T>>} The adapter instance
 */
export function create<T = AbstractDatabaseAdapter<any>, C extends IDatabaseAdapterInfo<any> = any>(config: C): Promise<AbstractDatabaseAdapter<T>>

/**
 * Creates a handle for the database adapter and configures it.
 * @param {string} adapter The identifier of the adapter to use
 * @param {C} config The configuration for the adapter
 * @returns {Promise<AbstractDatabaseAdapter<T>>} The adapter instance
 */
export function create<T = AbstractDatabaseAdapter<any>, C extends IDatabaseAdapterConfig = any>(adapter: string, config: C): Promise<AbstractDatabaseAdapter<T>>

/**
 * Creates a handle for the database adapter and configures it.
 * @param {IDatabaseAdapterConstructor<T>} adapter The adapter to use
 * @param {C} config The configuration for the adapter
 * @returns {Promise<AbstractDatabaseAdapter<T>>} The adapter instance
 */
export function create<T = AbstractDatabaseAdapter<any>, C extends IDatabaseAdapterConfig = any>(adapter: IDatabaseAdapterConstructor<T>, config: C): Promise<AbstractDatabaseAdapter<T>>

export function create<T = AbstractDatabaseAdapter<any>, C extends IDatabaseAdapterConfig | IDatabaseAdapterInfo<any> = any>(adapterOrNameOrConfig: IDatabaseAdapterConstructor<T> | string | C, configMaybe?: C): Promise<AbstractDatabaseAdapter<T>> {
	if(typeof adapterOrNameOrConfig === 'string') {
		const adapter: string = adapterOrNameOrConfig,
			  config: C       = configMaybe

		const constructor: IDatabaseAdapterConstructor<T> = databaseAdapters[adapter]
		if(!constructor) throw new Error(`Unknown / invalid database adapter: "${adapter}"`)

		return create<T, C>(databaseAdapters[adapter], config)
	} else if(!configMaybe) {
		const config: C | any = <C>adapterOrNameOrConfig

		return create<T, C>(config.type, config.config)
	} else {
		const adapter: IDatabaseAdapterConstructor<T> = <IDatabaseAdapterConstructor<T>>adapterOrNameOrConfig,
			  dbHandler: AbstractDatabaseAdapter<T>   = new adapter(configMaybe)

		return dbHandler.connect()
			.catch((err: Error) => {
				console.error(err.message)

				return dbHandler.handleUnreachable()
					.catch((err: Error) => {
						throw err
					})
			})
			.then(() => dbHandler.validate()
				.catch((err: Error) => {
					console.error(err.message)

					return dbHandler.repair()
						.catch((err: Error) => {
							if(err) throw err
						})
				}))
			.catch((err: Error) => {
				throw err
			})
			.then(() => { // I think putting the actual return operator here makes it more readable
				return dbHandler
			})
	}
}
