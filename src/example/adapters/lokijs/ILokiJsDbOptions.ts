import { PathLike } from 'fs'
import { IDatabaseAdapterConfig } from '../../../lib'


export interface ILokiJsDbOptions extends IDatabaseAdapterConfig {
	path: PathLike,
	schema?: string[]
}
