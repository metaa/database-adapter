/**
 * This is an excerpt of the db-adapter-lokijs module which was based on this example code.
 * If you want to use LokiJS, don't copy this example but rather check it out in the NPM registry:
 *
 * @see https://www.npmjs.com/package/db-adapter-lokijs
 */

import { LokiJsDbAdapter } from './LokiJsDbAdapter'


export * from './ILokiJsDbOptions'
export * from './LokiJsDbOptions'
export * from './LokiJsDbAdapter'

LokiJsDbAdapter.register()
