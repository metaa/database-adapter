export * from './lib'

if(!module.parent && !(process.env.NODE_ENV || '').startsWith('dev')) {
	console.error('This package is not directly runnable.\nPlease refer to the README.')
	process.exit(0)
}
